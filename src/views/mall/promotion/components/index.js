import SpuSelect from "./SpuSelect.vue";
import SpuAndSkuList from "./SpuAndSkuList.vue";

/**
 * 提供商品活动商品选择通用组件
 */
export { SpuSelect, SpuAndSkuList };
